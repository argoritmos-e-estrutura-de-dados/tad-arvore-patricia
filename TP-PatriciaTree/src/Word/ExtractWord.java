package Word;

/** @author messiah */

import PatriciaTree.PatriciaTree;
import PatriciaTree.PatriciaTree;
import java.util.StringTokenizer;
import java.io.*;

public class ExtractWord {
    private BufferedReader txt_file;
    public Word w;
    private String delimiters;
    private int line;
    
    public ExtractWord(String txt_file_name) throws FileNotFoundException, IOException{
        this.txt_file = new BufferedReader(new FileReader(txt_file_name));
        this.delimiters = " ";        
        this.w = null;
        this.line = 0;
    }
    
    public PatriciaTree nextWord() throws Exception{
        String txt_line;
        PatriciaTree pt = new PatriciaTree(128);
        StringTokenizer st = null;
        while((txt_line = txt_file.readLine()) != null){
            if(w == null || !st.hasMoreTokens()){
                if(txt_line == null)
                    return null;
                st = new StringTokenizer(txt_line.replaceAll("[()?:!.,;{}|]"," "), this.delimiters);
                int i = 1;
                line++;
                while(st.hasMoreTokens()){
                    this.w = new Word(st.nextToken(), i++, this.line);                    
                    //System.out.println("W: "+w.words+"\tLine: "+w.line+"\tCollum: "+w.collum);
                    pt.insert(w);
                }
            }
        }
        return pt;
    }
    public void closeFiles() throws Exception{
        this.txt_file.close();
    }
    
    public static int getIndex(String line, String i){
        return line.indexOf(i)+1;
    }
}

