
package PatriciaTree;

import Word.ExtractWord;
import java.util.ArrayList;
import javafx.util.Pair;

public class main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args){
         try{
            ExtractWord ew_0 = new ExtractWord(args[0]);
            System.out.println("Construindo a árvore para o arquivo: "+args[0]);
            PatriciaTree p = ew_0.nextWord();
            String[] s_0 = {"trabalho","computacao","governo","educacao","tecnologia","formacao","desenvolvimento","que","informatica","em","crise"};
            System.out.println("Pesquisa:");
            for(String s:s_0){
                System.out.println("\t\tComparações: "+p.search(s));
                p.number_of_comparisons = 0;
            }
            ew_0.closeFiles();
            
            
            ExtractWord ew_1 = new ExtractWord(args[1]);
            System.out.println("Construindo a árvore para o arquivo: "+args[1]);
            String[] s_1 = {"sociedade","software","ideia","pessoa","Informatica","etica","muito","ciencia","computacao","que","area","Moral"};
            System.out.println("Pesquisa:");
            for(String s:s_1){
                System.out.println("\t\tComparações: "+p.search(s));
                p.number_of_comparisons = 0;
            }
            ew_1.closeFiles();
            
        }catch(Exception e){
            System.out.println("Erro: "+e.getMessage());
        }
    }
    
}
