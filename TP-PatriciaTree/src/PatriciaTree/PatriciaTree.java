package PatriciaTree;

import Word.Word;
import java.util.ArrayList;

/** @author messiah */
public class PatriciaTree {
    public static abstract class Node{}
    
    public static class InnerNode extends Node{
        public int index;
        public Node left, right;
        
        /*public InnerNode(int i, Node left, Node right){
            this.index = i;            
            this.left = left;
            this.right = right;
        }*/
    }
    
    private static class OuterNode extends Node{
        Word key;
        public int number_of_ocurrences;       /** Indica o número de ocorrências da palavra. */
        public ArrayList<Word> ocurrences;    /** Indica todas as ocorrências da palavra. */
        
        /*public OuterNode(Word w){
            this.key = w;
            this.ocurrences = new ArrayList();
            this.ocurrences.add(w);
            this.number_of_ocurrences = 1;
        }*/
    }
    
    /** Raíz da árvore Patricia. */
    public Node root;
    /** Define o número de bits de cada elemento da árvore. */
    private int n_bits_key;
    /** Número de comparações na pesquisa. */
    public int number_of_comparisons;
    
    /** Construtor padrão. */
    public PatriciaTree(int n_bits_key){
        this.root = null;        
        this.n_bits_key = n_bits_key;
        this.number_of_comparisons = 0;
    }
    
    /** Método para criar nó interno. */
    private Node createInnerNode(int i, Node left, Node right){
        InnerNode in = new InnerNode();
        in.index = i;
        in.left = left;
        in.right = right;
        return in;        
    }
    
    /** Método para criar nó externo. */
    private Node createOuterNode(Word w){
        OuterNode on = new OuterNode();
        on.key = w;
        on.number_of_ocurrences = 1;
        on.ocurrences = new ArrayList();
        on.ocurrences.add(w);
        return on;
    }
    
    /** Método público para inserção. */
    public void insert(Word w){
        this.root = insert(w, this.root);
    }
    
    /** Método para inserção. */
    private Node insert(Word w, Node n){
        /** Se subárvore corrente for vazia, então é criado um nó externo contendo a
        chave w (isso ocorre somente na inserção da primeira chave) e o algoritmo termina. */
        if(n == null) {
            return this.createOuterNode(w);
        }else{
            /** Se a raiz da subárvore corrente for um nó
            interno, vai-se para a subárvore indicada pelo bit da chave k de índice
            dado pelo nó corrente. */
            Node p = n;
            char c_1 = '1';
            while(!this.isOuter(p)){
                InnerNode in = (InnerNode)p;
                //System.out.println(this.getByte(in.index, w.words));
                if(this.getByte(in.index, w.words) == c_1) p = in.right;
                else p = in.left;
            }
            OuterNode on = (OuterNode)p;
            
            int i = 1;
            /** Caso contrário, se a subárvore corrente for um nó externo, os bits da chave k são 
            comparados com os bits correspondentes da chave k deste nó externo até
            encontrar um índice i cujos bits defiram. */
            while((i < this.n_bits_key) && 
                    (this.getByte(i, w.words) == this.getByte(i, on.key.words))) i++;
            
            if(i > this.n_bits_key-1){
                System.out.println("Erro: chave já esta inserida.");                
                ((OuterNode)p).ocurrences.add(w);
                ((OuterNode)p).number_of_ocurrences++;
                return n;
            }else {
                return this.insertIn(w, n, i); //BITS DEFEREM!!!
            }
        }
    }
    /** Método público para pesquisa. */
    public int search(String w){
        this.search(w, this.root);
        return this.number_of_comparisons;
    }
    
    /** Método para pesquisa. */
    private void search(String s, Node n){
        if(this.isOuter(n)){
            number_of_comparisons+=2;
            OuterNode on = (OuterNode)n;
            if(s.intern() == on.key.words.intern()){                
                System.out.println("\tElemento: "+on.key.words+"\tNumero de ocorrências: "+on.number_of_ocurrences);
                for (Word w: on.ocurrences){
                    System.out.println("\t\tLinha: "+w.line+"\t\tColuna: "+w.collum);
                }
            }
            else 
               System.out.println("Elemento não encontrado.");            
        }else{
            InnerNode in = (InnerNode)n;
            number_of_comparisons++;
            if(this.getByte(in.index, s) ==  '1') 
                this.search(s, in.right);
            else 
                this.search(s, in.left);
        }
    }
    
    public void imprime(Node n){
        if(this.isOuter(n)){
            OuterNode on = (OuterNode)n; 
            System.out.println(on.key.words);
        }else{
            InnerNode in = (InnerNode)n;
            this.imprime(in.left);
            this.imprime(in.right);
        }        
    }
      
    private char getByte(int k, String w){
        String s = "";
        for (int i  = 0; i < w.length() && i < 128; i++)
              s += String.format("%08d", Integer.parseInt(Integer.toBinaryString((int)w.charAt(i))));        
        for(int j = s.length(); j < 128; j++)
            s += '0';
        return s.charAt(k);
    }
    
    /** Verifica se p é nó externo. */
    private boolean isOuter(Node n){
        Class c = n.getClass();
        return c.getName().equals(OuterNode.class.getName());
    }
    /** Verifica se p é nó interno. */
    private boolean isInner(Node n){
        Class c = n.getClass();
        return c.getName().equals(InnerNode.class.getName());
    }
    
    private Node insertIn(Word w, Node n, int i) {        
        InnerNode in = null;
        if(!this.isOuter(n)) 
            in = (InnerNode)n;
        /** A partir da raiz, a posição correta para a inserção do nó é pesquisada:
        ou é nó externo ou é um índice menor que o corrente. */
        if(this.isOuter(n) || (i < in.index)){  //O PROBLEMA PODE SER AQUI!!!!!SE NÃO DER CERTO!!!!
            /** Na posição correta é criado um novo nó externo com a chave a ser inserida. */
            Node p = this.createOuterNode(w);
            /** e um nó interno onde um filho é a chave corrente e o outro é a raiz corrente. */
            if(this.getByte(i, w.words) == '1') 
                return this.createInnerNode(i, n, p);
            else 
                return this.createInnerNode(i, p, n);
        }else{
            /** Se a posição não for a correta é realizada a busca na árvore recursivamente. */
            if(this.getByte(in.index, w.words) == '1')
                in.right = this.insertIn(w, in.right, i);
            else
                in.left = this.insertIn(w, in.left, i);
            return in;
        }
    }
    
    
    
    
    
}
    